export default {
    loginButtonInvisible(context) {
        context.commit('loginButtonInvisible');
    },
    textChange(context, payload) {
        context.commit('textChange', payload);
    },
    login() {
        VK.Auth.login();
    },
    friendsChange(context, payload) {
        context.commit('friendsChange', payload);
    },
    prevImg(context) {
        context.commit('prevImg');
    },
    nextImg(context) {
        context.commit('nextImg');
    },
    like(context) {
        context.commit('like');
    }
};