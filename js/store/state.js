export default {
    text: "Hi!",
    isLoginButtonVisible: true,
    isFriendsVisible: false,
    isPrevButtonDisabled: true,
    isNextButtonDisabled: true,
    isLikeTrue: false,
    friendsPosition: 0,
    friends: []
};