export default {
    loginButtonInvisible(state) {
        state.isLoginButtonVisible = false;
        return state;
    },
    textChange(state, text) {
        state.text = text;
        return state;
    },
    friendsChange(state, friends) {
        state.friends = friends;
        state.isFriendsVisible = friends.length > 0;
        state.friendsPosition = 0;
        if (state.friends.length > 1) {
            state.isNextButtonDisabled = false;
        }
        return state;
    },
    prevImg(state) {
        if (state.friendsPosition == 0) return state;
        state.friendsPosition = state.friendsPosition - 1;
        state.isPrevButtonDisabled = state.friendsPosition == 0;
        state.isNextButtonDisabled = state.friendsPosition == state.friends.length-1;
        state.isLikeTrue = false;
        return state;
    },
    nextImg(state) {
        if (state.friendsPosition == state.friends.length-1) return state;
        state.friendsPosition = state.friendsPosition + 1;
        state.isPrevButtonDisabled = state.friendsPosition == 0;
        state.isNextButtonDisabled = state.friendsPosition == state.friends.length-1;
        state.isLikeTrue = false;
        return state;
    },
    like(state) {
        state.isLikeTrue = !state.isLikeTrue; 
        return state;
    }
};