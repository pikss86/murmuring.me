import PubSub from './lib/pubsub.js';

const mediator = new PubSub();

export default mediator;