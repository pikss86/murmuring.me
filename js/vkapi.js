import mediator from './mediator.js';
import events from './events.js';

const api_version = "5.73"

mediator.subscribe(events.onload, () => {
    VK.init({
        apiId: 7450484
    });

    VK.Observer.subscribe("auth.sessionChange", function(r) {
        if (r.session.user) {
            let first_name = r.session.user.first_name;
            let last_name = r.session.user.last_name;
            let id = r.session.user.id;
            mediator.publish(events.onvklogin, { id, first_name, last_name });
        } else if (r.session) {
            let id = r.session.mid;
            VK.Api.call('users.get', {user_ids: r.session.mid, v: api_version}, function(r) {
                if(r.response) {
                    let first_name = r.response[0].first_name;
                    let last_name = r.response[0].last_name;
                    mediator.publish(events.onvklogin, { id, first_name, last_name });
                }
            });
        }
    });
    
    VK.Auth.getLoginStatus();

    mediator.subscribe(events.onstartloadfriends, ({ id }) => {
        VK.Api.call('friends.get', { user_id: id, fields: 'photo_400_orig', v: api_version }, r => {
            if (r.response && r.response.items) {
                mediator.publish(events.onendloadfriends, { friends: r.response.items });
            }
        });
    })
});

export default {}