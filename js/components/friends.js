import Component from '../lib/component.js';
import store from '../store/index.js';

export default class Friends extends Component {
    constructor() {
        super({
            store,
            element: document.querySelector('#idFriends')
        });
    }
  
    render() {
        const { friends, isFriendsVisible, 
            isPrevButtonDisabled, isNextButtonDisabled, isLikeTrue,
            friendsPosition } = store.state;
        const likeTrueImg = 'data:image/svg+xml;charset=utf-8,%3Csvg%20viewBox%3D%220%20' + 
        '0%2024%2024%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%3Cpath%20' + 
        'd%3D%22m0%200h24v24h-24z%22%20fill%3D%22none%22%2F%3E%3Cpath%20d%3D%22m17%202.9a6.43%20' + 
        '6.43%200%200%201%206.4%206.43c0%203.57-1.43%205.36-7.45%2010l-2.78%202.16a1.9%201.9%200%20' + 
        '0%201%20-2.33%200l-2.79-2.12c-6.05-4.68-7.45-6.47-7.45-10.04a6.43%206.43%200%200%201%20' + 
        '6.4-6.43%205.7%205.7%200%200%201%205%203.1%205.7%205.7%200%200%201%205-3.1z%22%20fill%3D%22' + 
        '%23ff3347%22%2F%3E%3C%2Fsvg%3E';
        const likeFalseImg = 'data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D%22http%3A%2F%2F' + 
        'www.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%2024%2024%22%3E%3Ctitle%3Elike_outline_24%3C' + 
        '%2Ftitle%3E%3Cpath%20d%3D%22M0%2C0H24V24H0Z%22%20fill%3D%22none%22%2F%3E%3Cpath%20d%3D%22' + 
        'M17%2C2.9A6.43%2C6.43%2C0%2C0%2C1%2C23.4%2C9.33c0%2C3.57-1.43%2C5.36-7.45%2C10l-2.78%2C2.16a1.9%2C' + 
        '1.9%2C0%2C0%2C1-2.33%2C0L8.05%2C19.37C2%2C14.69.6%2C12.9.6%2C9.33A6.43%2C6.43%2C0%2C0%2C1%2C7%2C' + 
        '2.9a6.46%2C6.46%2C0%2C0%2C1%2C5%2C2.54A6.46%2C6.46%2C0%2C0%2C1%2C17%2C2.9ZM7%2C4.7A4.63%2C4.63%2C' + 
        '0%2C0%2C0%2C2.4%2C9.33c0%2C2.82%2C1.15%2C4.26%2C6.76%2C8.63l2.78%2C2.16a.1.1%2C0%2C0%2C0%2C.12%2C' + 
        '0L14.84%2C18c5.61-4.36%2C6.76-5.8%2C6.76-8.63A4.63%2C4.63%2C0%2C0%2C0%2C17%2C4.7c-1.56%2C0-3%2C' + 
        '.88-4.23%2C2.73L12%2C8.5l-.74-1.07C10%2C5.58%2C8.58%2C4.7%2C7%2C4.7Z%22%20fill%3D%22%23828a99%22' + 
        '%2F%3E%3C%2Fsvg%3E';
        if (!isFriendsVisible) {
            this.element.innerHTML = '';
            return;
        }
        const friend = friends[friendsPosition];
        const imgs = `<img src="${friend.photo_400_orig}"/>`;
        this.element.innerHTML = `<section id="photoSection">`+
        `<div id="imgVK">${imgs}</div>` +
        `<div>` + 
        `<button id="idPrevButton" ${isPrevButtonDisabled ? 'disabled="true"' : ''}>Назад</button>` + 
        `<img id="idLike" src="${isLikeTrue ? likeTrueImg : likeFalseImg}" style="width: 50px"></img>` + 
        `<button id="idNextButton" ${isNextButtonDisabled ? 'disabled="true"' : ''}>Вперед</button>` + 
        `</div>`+
        `</section>`;
        const prevButtonElement = document.querySelector('#idPrevButton');
        const nextButtonElement = document.querySelector('#idNextButton');
        const likeImgElement = document.querySelector('#idLike');
        prevButtonElement.addEventListener('click', () => {
            store.dispatch('prevImg');
        });
        nextButtonElement.addEventListener('click', () => {
            store.dispatch('nextImg');
        });
        likeImgElement.addEventListener('click', () => {
            store.dispatch('like');
        });
    }
  }