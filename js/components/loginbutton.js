import Component from '../lib/component.js';
import store from '../store/index.js';

export default class LoginButton extends Component {
  constructor() {
    super({
      store,
      element: document.querySelector('#idLoginButton')
    });
  }

  render() {
    this.element.outerHTML = '<button id="idLoginButton" class="avtorization">Войти через VK</button>';
    this.element = document.querySelector('#idLoginButton');
    const loginButtonElement = document.querySelector('#idLoginButton');
    loginButtonElement.addEventListener('click', evt => {
        store.dispatch('login');
    });
    this.element.style.display = store.state.isLoginButtonVisible ? 'block' : 'none';
  }
}