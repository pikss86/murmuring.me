import Component from '../lib/component.js';
import store from '../store/index.js';

export default class WelcomeText extends Component {
  constructor() {
    super({
      store,
      element: document.querySelector('#idHi')
    });
  }

  render() {
    this.element.outerHTML = `<h1 id="idHi">${store.state.text}</h1>`;
    this.element = document.querySelector('#idHi');
  }
}