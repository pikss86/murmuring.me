import store from './store/index.js';
import mediator from './mediator.js';
import events from './events.js';
import vkapi from './vkapi.js';

import WelcomeText from './components/welcometext.js';
import LoginButton from './components/loginbutton.js';
import Friends from './components/friends.js';

window.onload = () => mediator.publish(events.onload);
mediator.subscribe(events.onvklogin, ({ id, first_name, last_name }) => {
    store.dispatch('textChange', "Hi, " + first_name + " " + last_name + "!");
    store.dispatch('loginButtonInvisible');
    mediator.publish(events.onstartloadfriends, { id });
})

mediator.subscribe(events.onendloadfriends, ({ friends }) => {
    store.dispatch('friendsChange', friends);
})

const welcomeText = new WelcomeText();
const loginButton = new LoginButton();
const friends = new Friends();

welcomeText.render();
loginButton.render();
friends.render();
